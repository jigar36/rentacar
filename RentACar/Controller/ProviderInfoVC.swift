//
//  ProviderInfoVC.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/31/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class ProviderInfoVC: UIViewController, NVActivityIndicatorViewable {
  
  // MARK: - Properties
    @IBOutlet weak var providerInfoTableView: UITableView!
    
    var rentersInfo = [String]()
    var URL_STRING = String()
    var providerInfoList = [ProviderInfo]()

    override func viewDidLoad() {
        super.viewDidLoad()
      
        let size = CGSize(width: 30, height: 30)
        startAnimating(size, message: "Loading...", type: .ballGridPulse)
      
        providerInfoTableView.delegate = self
        providerInfoTableView.dataSource = self
        
        self.navigationItem.title = "Renters List"
        self.navigationItem.rightBarButtonItem?.backgroundImage(for: .normal, style: .plain, barMetrics: .default)
        print(URL_STRING)
      
      
      let sortBtn = UIBarButtonItem(image: UIImage(named: "sortIcon"), style: .plain, target: self, action:  #selector(ProviderInfoVC.sortBtnPressed(_:)))
      self.navigationItem.rightBarButtonItem  = sortBtn
      
        downloadProviderData{
            self.stopAnimating()
            self.providerInfoTableView.reloadData()
          if self.providerInfoList.isEmpty{
            self.alertController("Opps No data found!", "Please check back with different dates")
          }
        }
    }

  // MARK: - Helper Methods
  
  @objc func sortBtnPressed(_ sender:UIBarButtonItem!){
    let alertController = UIAlertController(title: nil, message: "Select your sorting option!", preferredStyle: .actionSheet)
    
    let providerASC = UIAlertAction(title: "Provider Name Ascending", style: .default, handler: { (action) -> Void in
    
      let sortType = "providerASC"
      self.sortType(sortType)
      
    })
    
    let providerDESC = UIAlertAction(title: "Provider Name Descending", style: .default, handler: { (action) -> Void in
    
      let sortType = "providerDESC"
      self.sortType(sortType)
    })
    
    let carASC = UIAlertAction(title: "Car Availability Ascending", style: .default, handler: { (action) -> Void in

      let sortType = "carASC"
      self.sortType(sortType)
    })
    
    let carDESC = UIAlertAction(title: "Car Availability Descending", style: .default, handler: { (action) -> Void in

      let sortType = "carDESC"
      self.sortType(sortType)
    })
    
    let distanceASC = UIAlertAction(title: "Distance Ascending", style: .default, handler: { (action) -> Void in

      let sortType = "distanceASC"
      self.sortType(sortType)
    })
    
    let distanceDESC = UIAlertAction(title: "Distance Descending", style: .default, handler: { (action) -> Void in

      let sortType = "distanceDESC"
      self.sortType(sortType)
    })
    
    let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
    })
    
    alertController.view.tintColor = UIColor.darkGray
    cancelButton.setValue(UIColor.black, forKey: "titleTextColor")
    
    alertController.addAction(providerASC)
    alertController.addAction(providerDESC)
    alertController.addAction(carASC)
    alertController.addAction(carDESC)
    alertController.addAction(distanceASC)
    alertController.addAction(distanceDESC)
    alertController.addAction(cancelButton)
    
    self.navigationController!.present(alertController, animated: true, completion: nil)
  }
  
  func sortType(_ sortType: String){
    
    let size = CGSize(width: 30, height: 30)
    startAnimating(size, message: "Loading...", type: .ballGridPulse)
    
    switch sortType {
    case "providerASC":
      self.providerInfoList = self.providerInfoList.sorted(by: {$0.providerName < $1.providerName})
    case "providerDESC":
      self.providerInfoList = self.providerInfoList.sorted(by: {$0.providerName > $1.providerName})
    case "carASC":
      self.providerInfoList = self.providerInfoList.sorted(by: {$0.carsAvailable < $1.carsAvailable})
    case "carDESC":
      self.providerInfoList = self.providerInfoList.sorted(by: {$0.carsAvailable > $1.carsAvailable})
    case "distanceASC":
      self.providerInfoList = self.providerInfoList.sorted(by: {$0.distance < $1.distance})
    case "distanceDESC":
      self.providerInfoList = self.providerInfoList.sorted(by: {$0.distance > $1.distance})
    default:
      return
    }
    self.stopAnimating()
    self.providerInfoTableView.reloadData()
  }
    func downloadProviderData(completed: @escaping DownloadComplete){
        
        Alamofire.request(URL_STRING, method: .get).responseJSON { response in
            let result = response.result
            if let dict = result.value as? [String: AnyObject]{
                if let providerList = dict[JSONKeys.results.rawValue] as? [[String: AnyObject]]{
                    for obj in providerList{
                      let providerObj = ProviderInfo(obj, self.rentersInfo)
                        self.providerInfoList.append(providerObj)
                    }
                }
            }
            completed()
        }
    }
}

extension ProviderInfoVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return providerInfoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = providerInfoTableView.dequeueReusableCell(withIdentifier: "ProviderInfoCell", for: indexPath) as? ProviderInfoCell{
            
            if providerInfoList.isEmpty{
                return UITableViewCell()
            }
            let items = providerInfoList[indexPath.row]
            cell.configureCell(items)
            
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CarInfoVC") as! CarInfoVC
        myVC.providerInfo = providerInfoList[indexPath.row]
        myVC.rentersInfo = self.rentersInfo
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 155
    }
    
}
