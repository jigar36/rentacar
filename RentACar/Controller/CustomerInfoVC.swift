//
//  CustomerInfoVC.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/30/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit
import CoreLocation

class CustomerInfoVC: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var dateTimeTextField: UITextField!
    @IBOutlet weak var dropOffDateTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var continueBtn: UIButton!
  
    let locationManager = CLLocationManager()
    private var latitude = String()
    private var longitude = String()
    var rentersInfo = [String]()
    lazy var geocoder = CLGeocoder()
    let date = Date()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        addressTextField.delegate = self
      
        self.hideKeyboardWhenTappedAround()
        formatter.dateFormat = "yyyy-MM-dd"
        self.continueBtn.layer.cornerRadius = 20
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.activityIndicator.isHidden = true
        self.addressTextField.isHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    // MARK: - Actions
    @IBAction func continueBtnPressed(_ sender: UIButton) {
      if (self.addressTextField.text?.isEmpty)! || (self.dateTimeTextField.text?.isEmpty)! || (self.dropOffDateTextField.text?.isEmpty)!{
        self.alertController("Oops...!", "Please complete all the fields and try again.")
      }else if self.latitude.isEmpty{
          self.alertController("Wait a moment!", "Fetching Coordinates, Try Again... ")
      }else{
         self.navigateController()
      }
    }
    
    @IBAction func dateTimeTextfieldClicked(_ sender: UITextField) {
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(CustomerInfoVC.dismissPicker))
        dateTimeTextField.inputAccessoryView = toolBar
        
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.minimumDate = date
        dateTimeTextField.text = formatter.string(from: date)
        dateTimeTextField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(CustomerInfoVC.datePickerFromValueChanged), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func dropOffTextfieldClicked(_ sender: UITextField) {
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(CustomerInfoVC.dismissPicker))
        dropOffDateTextField.inputAccessoryView = toolBar
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        if !((self.dateTimeTextField.text?.isEmpty)!){
            if let minDate = formatter.date(from: self.dateTimeTextField.text!){
                datePickerView.minimumDate = minDate
                dropOffDateTextField.text = formatter.string(from: minDate)
            } else {
                datePickerView.minimumDate = date
                dropOffDateTextField.text = formatter.string(from: date)
            }
        }else{
            datePickerView.minimumDate = date
            dropOffDateTextField.text = formatter.string(from: date)
        }
        
        dropOffDateTextField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(CustomerInfoVC.datePickerFromValueChanged2), for: UIControlEvents.valueChanged)
    }
    
    @IBAction func currentLocationBtnClicked(_ sender: UIButton) {
        self.addressTextField.text = ""
        self.activityIndicator.isHidden = false
        self.addressTextField.isHidden = true
        self.activityIndicator.startAnimating()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    // MARK: - Helper Methods
    
    private func processResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        
        if let error = error {
            self.alertController("Error!", "Unable to Forward Geocode Address (\(error))")
            
        } else {
            var location: CLLocation?
            
            if let placemarks = placemarks, placemarks.count > 0 {
                location = placemarks.first?.location
            }
            
            if let location = location {
                let coordinate = location.coordinate
                self.latitude = "\(coordinate.latitude)"
                self.longitude = "\(coordinate.longitude)"
            }
        }
    }
    
    private func processResponse2(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        if let error = error {
            self.alertController("Error!", "Unable to Reverse Geocode Address (\(error))")
            
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                addressTextField.text = placemark.compactAddress
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
                self.addressTextField.isHidden = false
            }
        }
    }
    
    @objc func datePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateTimeTextField.text = dateFormatter.string(from: sender.date)
    }
    @objc func datePickerFromValueChanged2(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dropOffDateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    
    func navigateController(){
        let urlString = "\(API.API_URL)\(API.API_KEY)\(API.API_LATITUDE)\(self.latitude)\(API.API_LONGITUDE)\(self.longitude)\(API.API_RADIUS)\(API.API_PICKUP)\(String(describing: dateTimeTextField.text!))\(API.API_DROPOFF)\(String(describing: dropOffDateTextField.text!))"
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ProviderInfoVC") as! ProviderInfoVC
      
      self.rentersInfo.append(self.addressTextField.text!)
      self.rentersInfo.append(self.dateTimeTextField.text!)
      self.rentersInfo.append(self.dropOffDateTextField.text!)
      self.rentersInfo.append(self.latitude)
      self.rentersInfo.append(self.longitude)
      
      myVC.URL_STRING = urlString
      myVC.rentersInfo = self.rentersInfo
      self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}

extension CustomerInfoVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.latitude = "\(location.coordinate.latitude)"
            self.longitude = "\(location.coordinate.longitude)"
            let locationCoordinates = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            geocoder.reverseGeocodeLocation(locationCoordinates) { (placemarks, error) in
                // Process Response
                self.processResponse2(withPlacemarks: placemarks, error: error)
            }
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error \(error)")
    }
}

extension CustomerInfoVC: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let addressField = addressTextField.text else { return }
        // Geocode Address String
        geocoder.geocodeAddressString(addressField) { (placemarks, error) in
            // Process Response
            self.processResponse(withPlacemarks: placemarks, error: error)
        }
    }
}

extension UIToolbar {
  
  func ToolbarPiker(mySelect : Selector) -> UIToolbar {
    
    let toolBar = UIToolbar()
    
    toolBar.barStyle = UIBarStyle.default
    toolBar.isTranslucent = true
    toolBar.tintColor = UIColor.black
    toolBar.sizeToFit()
    
    let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: mySelect)
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    
    toolBar.setItems([ spaceButton, doneButton], animated: false)
    toolBar.isUserInteractionEnabled = true
    
    return toolBar
  }
}

extension CLPlacemark {
  
  var compactAddress: String? {
    if let name = name {
      var result = name
      
      if let street = thoroughfare {
        result += ", \(street)"
      }
      
      if let city = locality {
        result += ", \(city)"
      }
      
      return result
    }
    
    return nil
  }
  
}

extension UIViewController {
  func hideKeyboardWhenTappedAround() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
  }
  
  @objc func dismissKeyboard() {
    view.endEditing(true)
  }
  
  func alertController(_ title: String, _ message: String){
    let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
    let cancelAction = UIAlertAction(title: "OK", style: .default) { (result : UIAlertAction) -> Void in
      
    }
    alertController.addAction(cancelAction)
    self.present(alertController, animated: true, completion: nil)
  }
}


