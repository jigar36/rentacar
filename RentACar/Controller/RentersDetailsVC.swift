//
//  RentersDetailsVC.swift
//  RentACar
//
//  Created by Jigar Panchal on 4/1/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit

class RentersDetailsVC: UIViewController {

  // MARK: - Properties
    @IBOutlet weak var rentersUIView: RentersDetailsView!
    @IBOutlet weak var directionsBtn: UIButton!
  
  
    var providerInfo = ProviderInfo()
    var carInfo = CarsInfo()
    var rentersInfo = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.navigationItem.title = "Renter's Details"
      
        self.directionsBtn.layer.cornerRadius = 20
        self.configureUI(rentersInfo, providerInfo, carInfo)
    
    }

  func configureUI(_ rentersInfoIn: [String],_ providersInfo: ProviderInfo, _ carsInfo: CarsInfo){
        rentersUIView.configureViews(rentersInfoIn, providersInfo, carsInfo)
    }

    @IBAction func navigateBtnPressed(_ sender: UIButton) {
      
      let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NavigateVC") as! NavigateVC
      myVC.providerInfo = self.providerInfo
      myVC.rentersInfo = self.rentersInfo
      self.navigationController?.pushViewController(myVC, animated: true)
    }
}
