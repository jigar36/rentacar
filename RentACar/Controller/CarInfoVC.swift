//
//  CarInfoVC.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/31/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CarInfoVC: UIViewController, NVActivityIndicatorViewable {
  
    // MARK: - Properties
    var rentersInfo = [String]()
    var providerInfo = ProviderInfo()
    private var carInfoList = [CarsInfo]()

    @IBOutlet weak var carInfoCollectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Select a Car"
        carInfoCollectionView.delegate = self
        carInfoCollectionView.dataSource = self
      
      let sortBtn = UIBarButtonItem(image: UIImage(named: "sortIcon"), style: .plain, target: self, action:  #selector(CarInfoVC.sortBtnPressed(_:)))
      self.navigationItem.rightBarButtonItem  = sortBtn
    
        assignCarInfoObj {
            self.carInfoCollectionView.reloadData()
        }
        
    }
    
    func assignCarInfoObj(completed: @escaping DownloadComplete){
        
        for obj in providerInfo.carsLists{
            let carInfoObj = CarsInfo(obj)
            self.carInfoList.append(carInfoObj)
        }
        completed()
    }
  @objc func sortBtnPressed(_ sender:UIBarButtonItem!){
    let alertController = UIAlertController(title: nil, message: "Select your sorting option!", preferredStyle: .actionSheet)
    
    let priceLow = UIAlertAction(title: "Price Low to High", style: .default, handler: { (action) -> Void in
      
      let sortType = "priceLow"
      self.sortType(sortType)
      
    })
    
    let priceHigh = UIAlertAction(title: "Price High to Low", style: .default, handler: { (action) -> Void in
      
      let sortType = "priceHigh"
      self.sortType(sortType)
    })
    
    
    let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
    })
    
    alertController.view.tintColor = UIColor.darkGray
    cancelButton.setValue(UIColor.black, forKey: "titleTextColor")
    
    alertController.addAction(priceLow)
    alertController.addAction(priceHigh)
    alertController.addAction(cancelButton)
    
    self.navigationController!.present(alertController, animated: true, completion: nil)
  }

  func sortType(_ sortType: String){
    let size = CGSize(width: 30, height: 30)
    startAnimating(size, message: "Loading...", type: .ballGridPulse)
    
    
    switch sortType {
    case "priceLow":
      self.carInfoList = self.carInfoList.sorted(by: {$0.estimateAmount < $1.estimateAmount})
    case "priceHigh":
      self.carInfoList = self.carInfoList.sorted(by: {$0.estimateAmount > $1.estimateAmount})
        default:
      return
    }
    self.stopAnimating()
    self.carInfoCollectionView.reloadData()
  }

}

extension CarInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return carInfoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = carInfoCollectionView.dequeueReusableCell(withReuseIdentifier: "CarInfoCell", for: indexPath) as? CarInfoCell{
                let items = carInfoList[indexPath.row]
                cell.configureCell(items)
            return cell
        }else{
            
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "RentersDetailsVC") as! RentersDetailsVC
        myVC.providerInfo = self.providerInfo
        myVC.carInfo = carInfoList[indexPath.row]
        myVC.rentersInfo = self.rentersInfo
        self.navigationController?.pushViewController(myVC, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width / 2) - 6, height: 208)
    }
    
}
