//
//  NavigateVC.swift
//  RentACar
//
//  Created by Jigar Panchal on 4/2/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit
import MapKit

class NavigateVC: UIViewController {
  
  let regionRadius: CLLocationDistance = 1000
  let mapView = MKMapView()
  var providerInfo = ProviderInfo()
  var rentersInfo = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.frame = self.view.bounds
        self.mapView.delegate = self
        self.view.addSubview(mapView)
      
      let artwork = Artwork(title: "\(providerInfo.providerName)", locationName: "\(providerInfo.streetName), \(providerInfo.cityName), \(providerInfo.regionName)", coordinate: CLLocationCoordinate2D(latitude:  Double("\(providerInfo.latitude)")!, longitude: Double("\(providerInfo.longitude)")!))
      mapView.addAnnotation(artwork)
      

      let request = MKDirectionsRequest()
      request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: Double(rentersInfo[3])!, longitude: Double(rentersInfo[4])!), addressDictionary: nil))
      request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude:  Double("\(providerInfo.latitude)")!, longitude: Double("\(providerInfo.longitude)")!), addressDictionary: nil))
      request.requestsAlternateRoutes = false
      request.transportType = .automobile
      
      let directions = MKDirections(request: request)
      
      directions.calculate { [unowned self] response, error in
        guard let unwrappedResponse = response else { return }
        
        for route in unwrappedResponse.routes {
          self.mapView.add(route.polyline)
          self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
        }
      }
  }
}


extension NavigateVC: MKMapViewDelegate {

  func mapView(_ mapView: MKMapView,
               viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    if let annotation = annotation as? Artwork {
      let identifier = "artPin"
      if #available(iOS 11.0, *) {
        var view: MKMarkerAnnotationView
        view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
        view.canShowCallout = true
        view.calloutOffset = CGPoint(x: -5, y: 5)
        view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure) as UIView
        view.animatesWhenAdded = true
        view.markerTintColor = UIColor.red
        return view
      } else {
        // Fallback on earlier versions
      }
      if #available(iOS 11.0, *) {
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
          as? MKMarkerAnnotationView {
          dequeuedView.annotation = annotation
          view = dequeuedView
        } else {
        }
      } else {
        // Fallback on earlier versions
      }
    }
    return nil
  }
  
  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
    renderer.strokeColor = UIColor.blue
    renderer.alpha = 0.6
    return renderer
  }
  
  func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
    let location = view.annotation as! Artwork
    let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
    location.mapItem().openInMaps(launchOptions: launchOptions)
  }
  
}
