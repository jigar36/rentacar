//
//  ProviderInfoCell.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/31/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit

class ProviderInfoCell: UITableViewCell {

    @IBOutlet weak var providerNameLbl: UILabel!
    @IBOutlet weak var streetNameLbl: UILabel!
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var regionNameLbl: UILabel!
    @IBOutlet weak var carsAvailableLbl: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var distanceLbl: UILabel!
  
    
    func configureCell(_ productInfo: ProviderInfo){
        self.cellView.layer.cornerRadius = 10
        self.providerNameLbl.text = "Provider Name: \(productInfo.providerName)"
        self.streetNameLbl.text = "Street: \(productInfo.streetName)"
        self.cityNameLbl.text = "City: \(productInfo.cityName)"
        self.regionNameLbl.text = "Region: \(productInfo.regionName)"
        self.carsAvailableLbl.text = "Cars Available: \(productInfo.carsAvailable)"
        self.distanceLbl.text = "Distance: \(String(format: "%.2f", productInfo.distance)) miles"
    }
}
