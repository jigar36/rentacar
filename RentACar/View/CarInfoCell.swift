//
//  CarInfoCell.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/31/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit

class CarInfoCell: UICollectionViewCell {
    
    @IBOutlet weak var acrissCode: UILabel!
    @IBOutlet weak var transmissionLbl: UILabel!
    @IBOutlet weak var fuelLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var estimatedAmountLbl: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    func configureCell(_ carInfo: CarsInfo){
        self.cellView.layer.cornerRadius = 10
        self.acrissCode.text = "Ariss Code: \(carInfo.acrissCode)"
        self.transmissionLbl.text = "Transmission: \(carInfo.transmission)"
        self.fuelLbl.text = "Fuel: \(carInfo.fuel)"
        self.categoryLbl.text = "Category: \(carInfo.category)"
        self.typeLbl.text = "Type: \(carInfo.type)"
        self.priceLbl.text = "Price: $\(carInfo.price)/\(carInfo.rateType)"
        self.estimatedAmountLbl.text = "Est. Total: $\(carInfo.estimateAmount)"
    }
}
