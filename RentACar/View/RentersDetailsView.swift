//
//  RentersDetailsView.swift
//  RentACar
//
//  Created by Jigar Panchal on 4/1/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import UIKit

class RentersDetailsView: UIView {

    @IBOutlet weak var pickupDateLbl: UILabel!
    @IBOutlet weak var dropOffDateLbl: UILabel!
    @IBOutlet weak var userAddress: UILabel!
    
    
    @IBOutlet weak var providerName: UILabel!
    @IBOutlet weak var streetNameLbl: UILabel!
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var regionLbl: UILabel!
    
    @IBOutlet weak var acrissCodeLbl: UILabel!
    @IBOutlet weak var transmissionLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var estimatedCostLbl: UILabel!
    
    
    func configureViews(_ rentersInfoIn: [String], _ providersInfo: ProviderInfo, _ carsInfo: CarsInfo){
      
      self.userAddress.text = rentersInfoIn[0]
      self.pickupDateLbl.text = "Pickup date: \(rentersInfoIn[1])"
      self.dropOffDateLbl.text = "Dropoff date: \(rentersInfoIn[2])"
      
      self.providerName.text = "Provider Name: \(providersInfo.providerName)"
      self.streetNameLbl.text = providersInfo.streetName
      self.cityNameLbl.text = providersInfo.cityName
      self.regionLbl.text = providersInfo.regionName
        
      self.acrissCodeLbl.text = "AcrissCode: \(carsInfo.acrissCode)"
      self.transmissionLbl.text = "Transmission: \(carsInfo.transmission)"
      
      self.categoryLbl.text = "Category: \(carsInfo.category)"
      self.typeLbl.text = "Type: \(carsInfo.type)"
      self.priceLbl.text = "Price: $\(carsInfo.price)/\(carsInfo.rateType)"
      self.estimatedCostLbl.text = "Est. Amt: \(carsInfo.estimateAmount)"
    }
    
    
}
