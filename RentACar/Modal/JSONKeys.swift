//
//  JSONKeys.swift
//  RentACar
//
//  Created by Jigar Panchal on 4/2/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import Foundation

enum JSONKeys: String{
  
  case results = "results"
  case providerName = "provider"
  case companyName = "company_name"
  case branchId = "branch_id"
  case address = "address"
  case streetName = "line1"
  case cityName = "city"
  case regionName = "region"
  case cars = "cars"
  case location = "location"
  case latitude = "latitude"
  case longitude = "longitude"
  
  case vehicleInfo = "vehicle_info"
  case acrissCode = "acriss_code"
  case transmission = "transmission"
  case fuel = "fuel"
  case category = "category"
  case type = "type"
  
  case rates = "rates"
  case price = "price"
  case amount = "amount"
  case estAmount = "estimated_total"
}
