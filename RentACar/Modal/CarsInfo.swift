//
//  CarsInfo.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/31/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import Foundation

class CarsInfo{

    private var _acrissCode: String!
    private var _transmission: String!
    private var _fuel: String!
    private var _category: String!
    private var _type: String!
    private var _price: String!
    private var _estimatedAmount: String!
    private var _rateType: String!

    var acrissCode: String{
        if _acrissCode == nil
        {
            _acrissCode = "NA"
        }
        return _acrissCode
    }
    var transmission: String{
        if _transmission == nil
        {
            _transmission = "NA"
        }
        return _transmission
    }
    var fuel: String{
        if _fuel == nil
        {
            _fuel = "NA"
        }
        return _fuel
    }
    var category: String{
        if _category == nil
        {
            _category = "NA"
        }
        return _category
    }
    var type: String{
        if _type == nil
        {
            _type = "NA"
        }
        return _type
    }
    var price: String{
        if _price == nil
        {
            _price = "0.0"
        }
        return _price
    }
    var estimateAmount: String{
        if _estimatedAmount == nil
        {
            _estimatedAmount = "0.0"
        }
        return _estimatedAmount
    }
    var rateType: String{
        if _rateType == nil
        {
            _rateType = "NA"
        }
        return _rateType
    }
    
    init(){}
    
    init (_ carInfo: [String: Any]){
        if let vehicleInfo = carInfo[JSONKeys.vehicleInfo.rawValue] as? [String: Any]{
            self._acrissCode = vehicleInfo[JSONKeys.acrissCode.rawValue] as? String
            self._transmission = vehicleInfo[JSONKeys.transmission.rawValue] as? String
            self._fuel = vehicleInfo[JSONKeys.fuel.rawValue] as? String
            self._category = vehicleInfo[JSONKeys.category.rawValue] as? String
            self._type = vehicleInfo[JSONKeys.type.rawValue] as? String
        }
        if let rates = carInfo[JSONKeys.rates.rawValue] as? [[String: Any]]{
            self._rateType = rates[0][JSONKeys.type.rawValue] as? String
            
            if let prices = rates[0][JSONKeys.price.rawValue] as? [String: Any]{
                self._price = prices[JSONKeys.amount.rawValue] as? String
            }
        }
        if let estimatedTotal = carInfo[JSONKeys.estAmount.rawValue] as? [String: Any]{
            self._estimatedAmount = estimatedTotal[JSONKeys.amount.rawValue] as? String
        }
    }
    
}
