//
//  API.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/31/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import Foundation

struct API{
    static let API_URL = "https://api.sandbox.amadeus.com/v1.2/cars/search-circle?"
    static let API_KEY = "apikey=HC3Ukn6MsT5B37UCEnF4iK3e9dUza5bl"
    static let API_LATITUDE = "&latitude="
    static let API_LONGITUDE = "&longitude="
    static let API_RADIUS = "&radius=42"
    static let API_PICKUP = "&pick_up="
    static let API_DROPOFF = "&drop_off="
    
    //https://api.sandbox.amadeus.com/v1.2/cars/search-circle?apikey=HC3Ukn6MsT5B37UCEnF4iK3e9dUza5bl&latitude=35.1504&longitude=-114.57632&radius=42&pick_up=2018-03-31&drop_off=2018-04-02
}

typealias DownloadComplete = () -> ()
