//
//  ProviderInfo.swift
//  RentACar
//
//  Created by Jigar Panchal on 3/31/18.
//  Copyright © 2018 Jigar Panchal. All rights reserved.
//

import Foundation
import CoreLocation

class ProviderInfo{
    
    private var _providerName: String!
    private var _streetName: String!
    private var _cityName: String!
    private var _regionName: String!
    private var _branchCode: String!
    private var _carsAvailable: Int!
    private var _latitude: Double!
    private var _longitude: Double!
    private var _carsLists: [[String: Any]]!
    private var _distance: Double!

    
    var providerName: String{
        if _providerName == nil
        {
            _providerName = "NA"
        }
        return _providerName
    }
    var streetName: String{
        if _streetName == nil
        {
            _streetName = "NA"
        }
        return _streetName
    }
    var cityName: String{
        if _cityName == nil
        {
            _cityName = "NA"
        }
        return _cityName
    }
    var regionName: String{
        if _regionName == nil
        {
            _regionName = "NA"
        }
        return _regionName
    }
    var branchCode: String{
        if _branchCode == nil
        {
            _branchCode = "NA"
        }
        return _branchCode
    }
    var carsAvailable: Int{
        if _carsAvailable == nil
        {
            _carsAvailable = 0
        }
        return _carsAvailable
    }
    var latitude: Double{
        if _latitude == nil
        {
            _latitude = 0.0
        }
        return _latitude
    }
    var longitude: Double{
        if _longitude == nil
        {
            _longitude = 0.0
        }
        return _longitude
    }
    var distance: Double{
      if _distance == nil
      {
        _distance = 0.0
      }
      return _distance
    }
    var carsLists: [[String: Any]]{
        get{
            if _carsLists == nil
            {
                _carsLists = [["N/A": "N/A"]]
            }
            return _carsLists
        }
        set{
            _carsLists = newValue
        }
    }
    
    init(){}
    
  init(_ providerInfoList: [String: AnyObject], _ rentersInfo: [String]){
        
        if let provider = providerInfoList[JSONKeys.providerName.rawValue] as? [String: Any]{
            self._providerName = provider[JSONKeys.companyName.rawValue] as? String
        }
        if let id = providerInfoList[JSONKeys.branchId.rawValue] as? String{
            self._branchCode = id
        }
        if let address = providerInfoList[JSONKeys.address.rawValue] as? [String: Any] {
            self._streetName = address[JSONKeys.streetName.rawValue] as? String
            self._cityName = address[JSONKeys.cityName.rawValue] as? String
            self._regionName = address[JSONKeys.regionName.rawValue] as? String
        }
        if let carsList = providerInfoList[JSONKeys.cars.rawValue] as? [[String: Any]]{
            self._carsLists = carsList
            self._carsAvailable = carsList.count
        }
        if let location = providerInfoList[JSONKeys.location.rawValue] as? [String: Any]{
            self._latitude = location[JSONKeys.latitude.rawValue] as? Double
            self._longitude = location[JSONKeys.longitude.rawValue] as? Double
        }
    
        let coordinateFrom = CLLocation(latitude: Double(rentersInfo[3])!, longitude: Double(rentersInfo[4])!)
        let coordinateTo = CLLocation(latitude: self.latitude, longitude: self.longitude)
    
        let distanceInMeters = coordinateFrom.distance(from: coordinateTo)
        self._distance = distanceInMeters/1609.34
    }
    
}
